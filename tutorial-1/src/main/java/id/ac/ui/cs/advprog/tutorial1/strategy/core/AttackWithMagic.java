package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {
    public AttackWithMagic(){}
    @Override
    public String attack(){return "Avadakadabra!!";}
    @Override
    public String getType(){return "Attack with Magic";}
}
