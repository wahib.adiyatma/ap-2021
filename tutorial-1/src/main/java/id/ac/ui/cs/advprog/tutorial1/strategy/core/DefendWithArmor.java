package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {
    public DefendWithArmor(){}
    @Override
    public String defend(){return "i've got an Basilisk's armor!";}
    @Override
    public String getType(){return "Basilisk's Armor";}
}
