package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {

    public MysticAdventurer(Guild guild) {
        this.name = "Mystic";
        this.guild = guild;
        this.guild.add(this);
    }

    @Override
    public void update(){
        String questType = guild.getQuestType();
        if(questType.equals("D") || questType.equals("E")) {
            this.getQuests().add(guild.getQuest());
        }
    }
    //ToDo: Complete Me
}
