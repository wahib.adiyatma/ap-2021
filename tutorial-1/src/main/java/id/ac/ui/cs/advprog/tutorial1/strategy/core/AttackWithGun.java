package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithGun implements AttackBehavior {
    public AttackWithGun(){}

    @Override
    public String attack(){return "Dordordordordordor";}
    @Override
    public String getType(){return "Super Machine Gun";}
}
