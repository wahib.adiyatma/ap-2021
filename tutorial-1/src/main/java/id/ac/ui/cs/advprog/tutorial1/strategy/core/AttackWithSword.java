package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithSword implements AttackBehavior {
    public AttackWithSword(){}
    @Override
    public String attack(){return "Totsuka No Tsurugii!";}
    @Override
    public String getType(){return "Attack With Sword";}
}
