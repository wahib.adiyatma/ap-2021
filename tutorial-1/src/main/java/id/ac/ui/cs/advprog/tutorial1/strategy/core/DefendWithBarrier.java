package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {
    public DefendWithBarrier(){}
    @Override
    public String defend(){return "Bari Bari No Barierr!";}
    @Override
    public String getType(){return "Barier!";}
}
